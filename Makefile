# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/31 11:31:02 by pguillie          #+#    #+#              #
#    Updated: 2018/11/07 13:37:06 by pguillie         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME := libft_pbm.a

AR := ar

CC := gcc
WFLAGS := -Wall -Werror -Wextra
CFLAGS = $(WFLAGS)

LIBFT := libft/libft.a

INCDIR := includes/
SRCDIR := sources/
OBJDIR := objects/

HEADERS := $(addprefix $(INCDIR), \
	types/t_pbm.h \
	libft_pbm.h \
)

SOURCES := $(addprefix $(SRCDIR), \
	ft_pbm_alloc_row.c \
	ft_pbm_free_row.c \
	ft_pbm_get_dim.c \
	ft_pbm_get_plainformat.c \
	ft_pbm_read_init.c \
	ft_pbm_read_quit.c \
	ft_pbm_read_row.c \
	ft_pbm_set_dim.c \
	ft_pbm_set_plainformat.c \
	ft_pbm_write_init.c \
	ft_pbm_write_row.c \
	ft_pbm_write_quit.c \
)

OBJECTS = $(SOURCES:$(SRCDIR)%.c=$(OBJDIR)%.o)

LIBOBJ = $(addprefix $(OBJDIR), $(shell $(AR) t $(LIBFT) | grep .o))

.PHONY: all clean fclean re

all: $(NAME)

$(NAME): $(LIBFT) $(OBJDIR) $(OBJECTS)
	$(AR) rcs $@ $(OBJECTS)
	cp $(LIBFT) $(OBJDIR)
	cd $(OBJDIR) && $(AR) xs $(notdir $(LIBFT))
	$(AR) rs $@ $(LIBOBJ)

$(OBJDIR)%.o: $(SRCDIR)%.c $(HEADERS) Makefile
	$(CC) $(CFLAGS) -I$(INCDIR) -c -o $@ $<

$(OBJDIR):
	mkdir -p $(OBJDIR)

$(LIBFT):
	make -C $(dir $(LIBFT))

clean:
	make -C $(dir $(LIBFT)) clean
	rm -rf $(OBJDIR)

fclean: clean
	make -C $(dir $(LIBFT)) fclean
	rm -f $(NAME)

re: fclean all
