# libft_pbm

A simple library to handle Netpbm
["PBM"](http://netpbm.sourceforge.net/doc/pbm.html "PBM Format Documentation")
image files.

> As this library was intended to be used in 42 school projects,
it respects all the constraints dictated by the school (e.g. functions cannot
be longer than 25 lines and do...while loops are forbidden).


## Overview

This library allows reading and writing PBM image files.
The two versions of the PBM format are supported, i.e. _plain_ and _raw_
(see [the PBM Documentation](http://netpbm.sourceforge.net/doc/pbm.html
"PBM Format Documentation")).

Before actually reading/writing the raster of the image,
you need to initialize an `s_pbm` structure wich will read/write
the information (namely the magic number, the width and the height)
of the image.
You can achieve that by calling
[ft_pbm_read_init](README.md#documentation) or
[ft_pbm_write_init](README.md#documentation).

You also need to create an array of `char` to manipulate the rows of the raster.
[ft_pbm_alloc_row](README.md#documentation) will yield you an allocation of
the right size - that you should free through
[ft_pbm_free_row](README.md#documentation) when you are done using it.

Once you got these, you may read/write the raster one row at a time with
[ft_pbm_read_row](README.md#documentation) or
[ft_pbm_write_row](README.md#documentation).

After reading/writing, you should call
[ft_pbm_read_quit](README.md#documentation) or
[ft_pbm_write_quit](README.md#documentation)
to properly finish your action.


## Documentation

### SYNOPSIS
```c
#include <libft_pbm.h>

int ft_pbm_read_init(char *file, struct s_pbm *inpbm);
int ft_pbm_write_init(char *file, struct s_pbm *outpbm);

char *ft_pbm_alloc_row(t_pbm filepbm);
void ft_pbm_free_row(char *row);

int ft_pbm_read_row(t_pbm filepbm, char *row);
int ft_pbm_write_row(t_pbm filepbm, char *row);

int ft_pbm_get_dim(t_pbm filepbm, int *width, int *height);
int ft_pbm_set_dim(t_pbm *filepbm, int width, int height);

int ft_pbm_get_plainformat(t_pbm filepbm, int *plainformat);
int ft_pbm_set_plainformat(t_pbm *filepbm, int plainformat);

int ft_pbm_read_quit(struct s_pbm *inpbm);
int ft_pbm_write_quit(struct s_pbm *outpbm);
```

### DESCRIPTION
* **ft_pbm_read_init** initializes _inpbm_ with the information read in _file_.
* **ft_pbm_write_init** writes to _file_ the information in _outpbm_.	

* **ft_pbm_alloc_row** allocates a `char` array the size of _filepbm_'s rows.
* **ft_pbm_free_row** frees a `char` array previously allocated by the hereabove
function.

* **ft_pbm_read_row** reads _width_ pixels of the image and store them in the
_row_ array. A black pixel is `1`, a white one is `0`.
* **ft_pbm_write_row** writes the _width_ pixels of _row_ at the end of the
image file.

* **ft_pbm_get_dim** and **ft_pbm_set_dim** handle the _width_ and _height_ of
the image. Send a `NULL` pointer or a `0` value to avoid using the variable.

* **ft_pbm_get_plainformat** and **ft_pbm_set_plainformat** handle the version
of the PBM format. This is a boolean value that is `true (1)` for _plain_ format
and `false (0)` for _raw_ format.

* **ft_pbm_read_quit** and **ft_pbm_write_quit** reset the `s_pbm` structure.

### RETURN VALUE
All functions return `0` on success and a negative value on failure,
except for **ft_pbm_alloc_row** that returns the pointer to the allocated array
on success and `NULL` on failure, and **ft_pbm_free_row** that doesn't return
anything.
