/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pbm_read_init.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/01 15:39:12 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/03 16:05:28 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pbm.h"

static size_t		ft_pbm_read_comment(int fd, char *buf, size_t count)
{
	size_t	i;
	int		comment;

	i = 0;
	comment = 0;
	while (i < count)
	{
		if (read(fd, buf + i, 1) == 0)
			return (i);
		if (buf[i] == '#')
			comment = 1;
		else if (comment && (buf[i] == '\r' || buf[i] == '\n'))
			comment = 0;
		else if (!comment)
			i++;
	}
	return (i);
}

static int			ft_pbm_read_init_magic(int fd)
{
	char	magic_number[2];

	if (ft_pbm_read_comment(fd, magic_number, 2) < 2 || magic_number[0] != 'P')
		return (-1);
	if (magic_number[1] == '1')
		return (1);
	if (magic_number[1] == '4')
		return (0);
	return (-1);
}

static unsigned int	ft_pbm_read_init_dim(int fd)
{
	char			c;
	unsigned int	dim;
	unsigned int	t;

	if (ft_pbm_read_comment(fd, &c, 1) == 0)
		return (0);
	while (ft_isspace(c))
		if (ft_pbm_read_comment(fd, &c, 1) == 0)
			return (0);
	dim = 0;
	while (!ft_isspace(c))
	{
		if (!ft_isdigit(c))
			return (0);
		t = dim;
		dim = t * 10 + c - '0';
		if (dim < t)
			return (0);
		if (ft_pbm_read_comment(fd, &c, 1) == 0)
			return (0);
	}
	return (dim);
}

int					ft_pbm_read_init(char *file, t_pbm *data)
{
	int	fd;

	if (file == NULL || data == NULL)
		return (-1);
	if ((fd = open(file, O_RDONLY)) < 0)
		return (-1);
	if ((data->plain = ft_pbm_read_init_magic(fd)) < 0)
		return (-1);
	if ((data->width = ft_pbm_read_init_dim(fd)) == 0)
		return (-1);
	if ((data->height = ft_pbm_read_init_dim(fd)) == 0)
		return (-1);
	data->file = fd;
	return (0);
}
