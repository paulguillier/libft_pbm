/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pbm_write_row.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/03 11:17:07 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/03 16:03:17 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pbm.h"

static int	ft_pbm_write_row_plain(t_pbm data, char *row)
{
	char	byte[2];
	int		i;

	i = 0;
	while (i < data.width)
	{
		byte[0] = row[i++] + '0';
		byte[1] = '\n';
		if (write(data.file, byte, 2) < 0)
			return (-1);
	}
	return (0);
}

static int	ft_pbm_write_row_raw(t_pbm data, char *row)
{
	char	byte;
	int		bit;
	int		i;

	i = 0;
	while (i < data.width)
	{
		byte = 0;
		bit = 8;
		while (bit--)
		{
			byte |= row[i++] << bit;
		}
		if (write(data.file, &byte, 1) < 0)
			return (-1);
	}
	return (0);
}

int			ft_pbm_write_row(t_pbm data, char *row)
{
	if (data.plain)
		return (ft_pbm_write_row_plain(data, row));
	return (ft_pbm_write_row_raw(data, row));
}
