/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pbm_get_dim.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 15:58:35 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/03 11:17:58 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pbm.h"

int		ft_pbm_get_dim(t_pbm data, int *width, int *height)
{
	if (width)
		*width = data.width;
	if (height)
		*height = data.height;
	if (!data.width || !data.height)
		return (-1);
	return (0);
}
