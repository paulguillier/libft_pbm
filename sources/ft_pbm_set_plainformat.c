/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pbm_set_plainformat.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 18:35:33 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/03 11:17:58 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pbm.h"

int	ft_pbm_set_plainformat(t_pbm *data, int plainformat)
{
	if (data == NULL)
		return (-1);
	data->plain = !(!plainformat);
	return (data->plain);
}
