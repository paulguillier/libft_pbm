/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pbm_get_plainformat.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 18:32:55 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/03 11:17:58 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pbm.h"

int	ft_pbm_get_plainformat(t_pbm data, int *plainformat)
{
	if (plainformat)
		*plainformat = data.plain;
	return (data.plain);
}
