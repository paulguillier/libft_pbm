/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pbm_read_quit.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 10:27:02 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/03 16:05:08 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pbm.h"

int		ft_pbm_read_quit(t_pbm *data)
{
	int	ret;

	if (data == NULL)
		return (-1);
	ret = close(data->file);
	data->file = -1;
	data->width = 0;
	data->height = 0;
	data->plain = 0;
	return (ret);
}
