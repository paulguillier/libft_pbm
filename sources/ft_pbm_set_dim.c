/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pbm_set_dim.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 16:36:30 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/03 11:17:58 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pbm.h"

int		ft_pbm_set_dim(t_pbm *data, int width, int height)
{
	if (data == NULL)
		return (-1);
	if (width)
		data->width = width;
	if (height)
		data->height = height;
	return (0);
}
