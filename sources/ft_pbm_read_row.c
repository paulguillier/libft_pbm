/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pbm_read_row.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 11:21:03 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/07 13:55:44 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pbm.h"

static int	ft_pbm_read_row_plain(t_pbm data, char *row)
{
	char	c;
	int		i;

	i = 0;
	while (i < data.width)
	{
		if (read(data.file, &c, 1) == 0)
			return (-1);
		while (ft_isspace(c))
			if (read(data.file, &c, 1) == 0)
				return (-1);
		if (c == '0' || c == '1')
			row[i++] = c - '0';
		else
			return (-1);
	}
	return (0);
}

static int	ft_pbm_read_row_raw(t_pbm data, char *row)
{
	char	c;
	int		i;
	int		j;

	i = 0;
	while (i < data.width)
	{
		if (read(data.file, &c, 1) == 0)
			return (-1);
		j = 8;
		while (j--)
			row[i++] = (c & (1 << j) ? 1 : 0);
	}
	return (0);
}

int			ft_pbm_read_row(t_pbm data, char *row)
{
	if (data.file < 0 || row == NULL)
		return (-1);
	if (data.plain)
		return (ft_pbm_read_row_plain(data, row));
	return (ft_pbm_read_row_raw(data, row));
}
