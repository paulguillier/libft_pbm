/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pbm_write_init.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 15:38:33 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/03 16:29:02 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pbm.h"

static int	ft_pbm_write_init_magic(int fd, int plain)
{
	if (plain)
		return (write(fd, "P1\n", 3));
	return (write(fd, "P4\n", 3));
}

static int	ft_pbm_write_init_comment(int fd, char *file)
{
	if (write(fd, "# ", 2) < 0)
		return (-1);
	if (write(fd, file, ft_strlen(file)) < 0)
		return (-1);
	if (write(fd, " - created with the libft_pbm\n", 30) < 0)
		return (-1);
	return (0);
}

static int	ft_pbm_write_init_dim(int fd, int width, int height)
{
	if (ft_putnbr_fd(width, fd) < 0)
		return (-1);
	if (write(fd, " ", 1) < 0)
		return (-1);
	if (ft_putnbr_fd(height, fd) < 0)
		return (-1);
	if (write(fd, "\n", 1) < 0)
		return (-1);
	return (0);
}

int			ft_pbm_write_init(char *file, t_pbm *data)
{
	int	fd;

	if (file == NULL || data == NULL || data->width == 0 || data->height == 0)
		return (-1);
	if ((fd = open(file, O_WRONLY | O_CREAT | O_TRUNC, 0644)) < 0)
		return (-1);
	if (ft_pbm_write_init_magic(fd, data->plain) < 0)
		return (-1);
	if (ft_pbm_write_init_comment(fd, file) < 0)
		return (-1);
	if (ft_pbm_write_init_dim(fd, data->width, data->height) < 0)
		return (-1);
	data->file = fd;
	return (0);
}
