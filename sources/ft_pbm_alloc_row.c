/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pbm_alloc_row.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 16:00:58 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/03 11:27:16 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pbm.h"

char	*ft_pbm_alloc_row(t_pbm data)
{
	return ((char *)malloc(sizeof(char) * data.width));
}
