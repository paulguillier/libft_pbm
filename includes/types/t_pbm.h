/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_pbm.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 19:55:24 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/03 16:04:54 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_PBM_H
# define T_PBM_H

struct	s_pbm
{
	int		file;
	int		width;
	int		height;
	int		plain;
};

typedef struct s_pbm	t_pbm;

#endif
