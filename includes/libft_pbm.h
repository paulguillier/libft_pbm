/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_pbm.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/03 11:18:36 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/07 15:18:12 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PBM_H
# define LIBFT_PBM_H

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <stdint.h>

# include "types/t_pbm.h"
# include "libft/libft.h"

int		ft_pbm_read_init(char *file, t_pbm *data);
int		ft_pbm_read_row(t_pbm data, char *row);
int		ft_pbm_read_quit(t_pbm *data);

int		ft_pbm_write_init(char *file, t_pbm *data);
int		ft_pbm_write_row(t_pbm data, char *row);
int		ft_pbm_write_quit(t_pbm *data);

char	*ft_pbm_alloc_row(t_pbm data);
void	ft_pbm_free_row(char *row);

int		ft_pbm_get_dim(t_pbm data, int *width, int *height);
int		ft_pbm_set_dim(t_pbm *data, int width, int height);

int		ft_pbm_get_plainformat(t_pbm data, int *plainformat);
int		ft_pbm_set_plainformat(t_pbm *data, int plainformat);

#endif
